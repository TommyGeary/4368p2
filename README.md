> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Tommy Geary

### Project 2 Requirements:

*Three Parts*

1. Clone starter files
2. Edit and compile java files
3. Questions

#### README.md file should include the following items:

* P2 assignment requirements
* Screenshots of pre and post validation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of pre-valid form entry*:

![pre-valid Screenshot](prevalid.png)

![post-valid Screenshot](valid.png)

![table screenshot](table.png)
![update screenshot](update.png)
![changed screenshot](changed.png)
![sql screenshot](sql.png)




